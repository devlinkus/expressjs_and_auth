var express = require('express')
var router = express.Router()
var mailer = require('../mailer/mail.js')
var options = require('../config/config.js').mysql
var mysql = require('mysql')
var connection = mysql.createConnection(options)
var users = require('../models/users')
var User = new users(connection)
var isAuthorized = require('../middleware/security.js').isAuthorized((connection))

/*
router.get('/', (req, res) => {res.send('aaa');});
router.get('/login', (req, res) => {res.send('login');});
router.post('/login', (req, res) => {res.send('aaa');});
router.get('/logout', (req, res) => {res.send('logout');});
router.get('/signin', (req, res) => {res.send('signin');});
router.post('/signin', (req, res) => {res.send('aaa');});
router.get('/account_activation/:hash', (req, res) => {res.send('account_activation/:hash');});
router.get('/account_recovery/:hash', (req, res) => {res.send('account_recovery/:hash');});
router.get('/account_recovery', (req, res) => {res.send('account_recovery');});
router.post('/account_recovery', (req, res) => {res.send('aaa');});
router.get('/profile', (req, res) => {res.send('aaa');});
*/

router.get('/', (req, res) => res.end(req.protocol))

/****************/
router.get('/login', (req, res) =>
{
  var data = `
  <h2>Log In</h2>
  <form action="/login" method="POST">
    <input type="email" name="username" placeholder="email"/>
    <input type="password" name="password" placeholder="password"/>
    <input type="submit" value="Submit"/>
  </form>
  <a href="/account_recovery">account_recovery</a>
  `
  res.send(data)
})
/****************/

router.post('/login', (req, res) =>
{
  req.assert('username', 'Username cannot be empty').notEmpty()
  req.assert('password', 'Password cannot be empty').notEmpty()
  var errors = req.validationErrors()
  if (errors)
    return res.send(errors)

  User.logIn(req.body.username, req.body.password, req.session.id)
    .then(rsp => res.send(rsp))
    .catch(err => res.send(err))
})

router.get('/logout', (req, res) =>
{
  User.logout(req.session.id)
    .finally(() =>
    {
      req.session.regenerate(() => res.send('logout ok'))
    })

})

/**
 * Account registration
 */
/****************/
router.get('/signin', (req, res) =>
{
  var data = `
  <h2>Sign In</h2>
  <form action="/signin" method="POST">
    <input type="email" name="username" placeholder="email"/>
    <input type="text" name="password" placeholder="password"/>
    <input type="submit" value="Submit"/>
  </form>
  `
  res.send(data)
})
/****************/

/**
 * Create a new account and send an activation mail
 */
router.post('/signin', (req, res) =>
{
  req.assert('username', 'Email is not valid').isEmail()
  req.assert('password', 'Password cannot be empty').notEmpty()
  var errors = req.validationErrors()
  if (errors)
    return res.send(errors)

  User.create(req.body.username, req.body.password, mailer.genHash())
    .then(rsp =>
    {
      let link = req.protocol + '://' + req.get('host') + '/account_activation/' + rsp.account_verification
      let username = rsp.username
      mailer.send(mailer.mailActivation(username, link))
        .then(rsp => res.send('Un mail de confirmation à ' + username))
        .catch(err => res.send(err))
    })
    .catch(err => res.send(err))
})

/**
 * Active an account and go to log
 */
router.get('/account_activation/:hash', (req, res) =>
{
  req.assert('hash', 'Password cannot be empty').notEmpty()
  var errors = req.validationErrors()
  if (errors)
    res.send(errors)

  User.activeAcount(req.params.hash)
    .then(rsp => res.send('activation ok, go to log'))
    .catch(err => res.send(err))

})

router.get('/account_recovery/:hash', (req, res) =>
{
  req.assert('hash', 'Password cannot be empty').notEmpty()
  var errors = req.validationErrors()
  if (errors)
    return res.send(errors)

  User.findMailByHash(req.params.hash, req.session.id)
    .then(mail =>
    {
      var data = `
      <a h="${mail}">${mail}</a>  
      <form action="/account_new_pwd" method="POST">
      <input type="hidden" name="hash" value="${req.params.hash}"/>
      <input type="password" name="password" placeholder="password"/>
      <input type="submit" value="Submit"/>
      </form>
      `
      res.send(data)
    })
    .catch(err => res.send(err))
})

/****************/
router.get('/account_recovery', (req, res) =>
{
  var data = `
  <h2>Account recovery</h2>
  <form action="/account_recovery" method="POST">
    <input type="email" name="username" placeholder="email"/>
    <input type="submit" value="Submit"/>
  </form>
  `
  res.send(data)
})
/****************/

router.post('/account_recovery', (req, res) =>
{
  req.assert('username', 'Email is not valid').isEmail()
  var errors = req.validationErrors()
  if (errors)
    return res.send(errors)

  User.passwordRecovery(req.body.username, mailer.genHash())
    .then(rsp =>
    {
      let link = req.protocol + '://' + req.get('host') + '/account_recovery/' + rsp.account_verification
      let username = rsp.username
      mailer.send(mailer.mailRecovery(username, link))
        .then(() => res.end('Un mail de réinitialisation à été envoyé à ' + username))
        .catch(err => res.send(err))
    })
    .catch(err => res.send(err))
})

router.post('/account_new_pwd', function (req, res)
{
  req.assert('hash', 'Request not valid').notEmpty()
  req.assert('password', 'Password is empty').notEmpty()
  var errors = req.validationErrors()
  if (errors)
    return res.send(errors)

  User.passwordRecoveryUpdate(req.body.password, req.body.hash)
    .then(() => res.send('go log'))
    .catch(err => res.send('/login_recovery'))

})

router.get('/profile', isAuthorized, (req, res) => {res.send(req.profile)})

module.exports = router
