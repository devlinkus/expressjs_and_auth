var bcrypt = require('bcrypt')

class User
{
  constructor (conn)
  {
    this.conn = conn
  }

  create (username, password, mail_hash)
  {
    return new Promise((resolve, reject) =>
    {
      bcrypt.hash(password, 10)
        .then(hash =>
        {
          var user = {username: username, password: hash, account_verification: mail_hash}
          this.conn.query('INSERT INTO users SET ?', user, (error, results, fields) =>
          {
            if(error || results.insertId === undefined || results.warningCount !== 0)
              return reject(error ? error: results)
            resolve({username: username, account_verification: mail_hash})
          })
        })
    })
  }

  logIn (username, password, token)
  {
    return new Promise((resolve, reject) =>
    {
      this.conn.query('SELECT id, password, username FROM users WHERE username = ? LIMIT 1', [username], (error, results, fields) =>
      {
        if (error || results.length < 1)
          return reject(error)
        var username = results[0].username;
        bcrypt.compare(password, results[0].password).then(res =>
        {
          if (!res)
            return reject('Incorrect password')
          this.conn.query('UPDATE users SET token = ? WHERE id = ?', [token, results[0].id], (error, results, fields) =>
          {
            error ? reject(error) : resolve(username)
          })
        })
      })
    })
  }

  logout (sessid)
  {
    return new Promise((resolve, reject) =>
    {
      this.conn.query('UPDATE users SET token = NULL WHERE token = ? LIMIT 1', [sessid], (error, results, fields) =>
      {
        error ? reject(error) : resolve(results)
      })
    })
  }

  activeAcount (mail_hash, stat = true)
  {
    return new Promise((resolve, reject) =>
    {
      this.conn.query('UPDATE users SET enable = ?, account_verification = NULL WHERE account_verification = ? LIMIT 1', [stat, mail_hash], (error, results, fields) =>
      {
        if (error || results.changedRows === 0)
          return reject(error ? error: results)
        resolve(results)
      })
    })
  }

  passwordRecovery (username, mail_hash = 'none')
  {
    return new Promise((resolve, reject) =>
    {
      this.conn.query('SELECT id FROM users WHERE username = ? LIMIT 1', [username], (error, results, fields) =>
      {
        if (error || results.length < 1 || results[0].id === undefined)
          return reject('mail not found')
        this.conn.query('UPDATE users SET account_verification = ?, token = NULL WHERE id = ?', [mail_hash, results[0].id], (error, results, fields) =>
        {
          if(error || results.affectedRows === 0 || results.warningCount !== 0)
            return reject(error ? error: results)
          resolve({username: username, account_verification: mail_hash})
        })
      })
    })
  }

  findMailByHash (mail_hash = 'none')
  {
    return new Promise((resolve, reject) =>
    {
      this.conn.query('SELECT username FROM users WHERE account_verification = ? LIMIT 1', [mail_hash], (error, results, fields) =>
      {
        if (error || results.length < 1 || results[0].username === undefined)
          return reject('mail not found')
        resolve(results[0].username)
      })
    })
  }

  passwordRecoveryUpdate (password, mail_hash = 'none')
  {
    return new Promise((resolve, reject) =>
    {
      bcrypt.hash(password, 10)
        .then(hash =>
        {
          this.conn.query('UPDATE users SET password = ?, token = NULL, account_verification = NULL WHERE account_verification = ? LIMIT 1', [hash, token, mail_hash], (error, results, fields) =>
          {
            if (error || results.changedRows === 0)
              return reject(error)
            resolve(results)
          })
        })
    })
  }
}

module.exports = User
