
// MYSQL

module.exports.mysql = {
  host     : process.env.DB_HOST,
  user     : process.env.DB_USER,
  password : process.env.DB_PASS,
  database : process.env.DB_NAME
}

// MySQLStore

module.exports.sessionStore = function(store){
  return {
    key               : 'session_cookie_name',
    secret            : 'session_cookie_secret',
    store             : store,
    resave            : false,
    saveUninitialized : true,
  }
}

// Nodemailer - Owner account

module.exports.mailerTransport = {
  host:     process.env.MAIL_HOST,
  port:     process.env.MAIL_PORT,
  auth: {
    user:   process.env.MAIL_AUTH_USER,
    pass:   process.env.MAIL_AUTH_PASS
  }
}

