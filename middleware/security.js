
module.exports.isAuthorized = function(connection)
{
  return function(req, res, next)
  {
    connection.query('SELECT * FROM users WHERE token = ?', [req.session.id], function (error, results, fields)
    {
      if (error || results.length < 1)
        res.send('no authorized')
      else
      {
        console.log(results)
        req.profile = results
        next()
      }
    })
  }
}