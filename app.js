var express = require('express');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var mysql = require('mysql');
var expressValidator = require('express-validator');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var config = require('./config/config.js');
var connection = mysql.createConnection(config.mysql);
var sessionStore = new MySQLStore({}, connection);

var app = express()
if (process.env.DEBUG == true)
{
  app.use(logger(process.env.DEBUG_FORMAT));
}
app.use(expressValidator());
app.use(session(config.sessionStore(sessionStore)));
app.use(express.json());
app.use(express.urlencoded({extended: false}))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

app.use('/', indexRouter);
app.use('/users', usersRouter);

module.exports = app
