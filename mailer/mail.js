'use strict'
var nodemailer = require('nodemailer')
var configOwner = require('../config/config.js').mailerTransport;
var configAccountActivation = require('../config/config.js').mailerTransport;

module.exports.genHash = () => {
  var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  var token = '';
  for (var i = 16; i > 0; --i) {
    token += chars[Math.round(Math.random() * (chars.length - 1))];
    token += chars[Math.round(Math.random() * (chars.length - 1))];
    token += chars[Math.round(Math.random() * (chars.length - 1))];
  }
  return token
}

module.exports.send = function (mailOptions)
{
  return new Promise((resolve, reject) =>
  {
    nodemailer.createTestAccount((err, account) =>
    {
      const transporter = nodemailer.createTransport(configOwner)
      transporter.sendMail(mailOptions, (error, info) =>
      {
        console.log(mailOptions)
        if (error)
          return reject(error)
        resolve()
      })
    })
  })
}

module.exports.mailActivation = function(to, link){
  return {
    from: process.env.MAIL_FROM,
    to: 'user@lol.com',
    subject: 'activation de votre compte',
    text: 'account: ' + link,
    html: '<em>account activation</em>:<br><a href="' + link + '" target="_blank">' + link + '</a>',
  }
}

module.exports.mailRecovery = function(to, link){
  return {
    from: process.env.MAIL_FROM,
    to: to,
    subject: 'récupération de votre compte',
    text: 'account: ' + link,
    html: '<em>account recovery</em>:<br><a href="' + link + '" target="_blank">' + link + '</a>',
  }
}

